module SpotifySharedPlaylist {
    requires javafx.controls;
    requires javafx.fxml;
    requires spring.beans;
    requires spring.core;
    requires spring.context;
    requires spring.web;
    requires spring.webmvc;
    requires spring.boot;
    requires spring.boot.starter;
    requires spring.aop;
    requires spring.expression;
    requires spring.jcl;
    requires spring.boot.autoconfigure;
    requires mongo.java.driver;
    requires spring.data.commons;
    requires spring.data.mongodb;
    requires java.validation;
    requires org.apache.commons.lang3;
    requires spring.security.core;
    requires org.apache.httpcomponents.httpcore;
    requires org.apache.httpcomponents.httpclient;
}