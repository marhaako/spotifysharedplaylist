package spotify_shared_playlists.Controllers;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spotify_shared_playlists.Util.Util;
import spotify_shared_playlists.models.Playlist;
import spotify_shared_playlists.models.Song;
import spotify_shared_playlists.repositories.PlaylistRepository;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/en/playlist")
public class PlaylistController {
    @Autowired
    private PlaylistRepository repository;

    private final AtomicInteger songID = new AtomicInteger();


    @RequestMapping(value = "/", method = POST)
    public Playlist newPlaylist(@Valid @RequestBody Playlist playlist) {
        playlist.set_id(ObjectId.get());
        repository.save(playlist);
        return playlist;
    }

    @RequestMapping(value = "/", method = GET)
    public List<Playlist> getAllPlaylists() {
        return repository.findAll();
    }

    @RequestMapping(value = "/{playlistId}", method = GET)
    public List<Song> getAllSongsFromPlaylist(@PathVariable("playlistId") ObjectId playlistId) {
        Playlist playlist = repository.findBy_id(playlistId);
        return playlist.getSongs();
    }

    @RequestMapping(value = "/{playlistId}", method = POST)
    public Playlist addSongToPlaylist(@PathVariable("playlistId") ObjectId playlistId, @Valid @RequestBody Song song) {

        Playlist playlist = repository.findBy_id(playlistId);
        playlist.addSong(song);
        repository.save(playlist);

        return playlist;
    }

    @RequestMapping(value = "/{playlistId}/skip", method = GET)
    public Playlist nextSongInPlaylist(@PathVariable("playlistId") ObjectId playlistId) {
        Playlist playlist = repository.findBy_id(playlistId);
        playlist.nextSong();
        repository.save(playlist);
        return playlist;
    }

    @RequestMapping(value = "/{playlistId}/remove/{songIndex}")
    public Playlist removeSongFromPlaylist(@PathVariable("playlistId") ObjectId playlistId, @PathVariable("songIndex") int songIndex) {
        Playlist playlist = repository.findBy_id(playlistId);
        playlist.removeSong(songIndex);
        repository.save(playlist);
        return playlist;
    }


    @RequestMapping(value = "/disp")
    public String loadPage() {
        return
                "  <div class=\"g-recaptcha\"\n" +
                "    data-sitekey=\"6LcWvb0UAAAAABiEKhto7jE-9am_yhMrOu47yaTo\">\n" +
                "  </div>";
    }

}


