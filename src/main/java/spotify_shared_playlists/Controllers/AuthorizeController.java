package spotify_shared_playlists.Controllers;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/en/authorize")
public class AuthorizeController {

    @RequestMapping(value = "", method = POST)
    public String authorize() {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpGet request = new HttpGet("https://accounts.spotify.com/authorize?" +
                "client_id=db55b25827384acd933ebc6486161352" +
                "&response_type=code" +
                "&redirect_uri=");
        try (CloseableHttpResponse response = httpClient.execute(request)) {

            // Get HttpResponse Status
            System.out.println(response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();
            Header headers = entity.getContentType();

            if (entity != null) {
                String result = EntityUtils.toString(entity);
                return result;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
