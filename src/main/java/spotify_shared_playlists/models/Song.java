package spotify_shared_playlists.models;

import org.springframework.data.annotation.Id;
import org.bson.types.ObjectId;
public class Song {
    private String _id;

    public Song() {}

    public Song(String _id) {
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
