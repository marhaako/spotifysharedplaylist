package spotify_shared_playlists.models;

import org.springframework.data.annotation.Id;
import org.bson.types.ObjectId;

import java.util.ArrayList;

public class Playlist {
    @Id
    private ObjectId _id;

    private String name;
    private ArrayList<Song> songs;

    public Playlist() {}

    public Playlist(ObjectId _id, String name) {
        this._id = _id;
        this.name = name;
        songs = new ArrayList<>();

    }


    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Song> getSongs() {
        return songs;
    }

    public void addSong(Song song) {
        songs.add(song);
    }

    public void nextSong() {
        songs.remove(0);
    }

    public void removeSong(int index) {
        songs.remove(index);
    }
}
