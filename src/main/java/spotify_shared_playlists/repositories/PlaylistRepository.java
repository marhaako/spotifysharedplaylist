package spotify_shared_playlists.repositories;

import org.bson.types.ObjectId;
import spotify_shared_playlists.models.Playlist;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface PlaylistRepository extends MongoRepository<Playlist, String> {
    Playlist findBy_id(ObjectId _id);
}
