package spotify_shared_playlists.Util;

import java.net.URI;
import java.net.URISyntaxException;

public class Util {

    public String getPieceOfURI(String uri, int index) {
        try {
            URI Uri = new URI(uri);
            String[] pieces = Uri.getPath().split("/");
            return pieces[index];
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
