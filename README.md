# Spotify shared playlist
An ongoing project where I want to build an API which allows for several Spotify users to add songs to a common queue.

The client will be web based, and allows users to create these so called shared queues. A unique key will be generated for each created playlist,
allowing others with access to this key to join the queue and add songs to it. 

Uses the Spotify Web API.
